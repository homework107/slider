const upBtn = document.getElementById('up-button')
const downBtn = document.getElementById('down-button')
const sidebar = document.getElementById('sidebar')
const mainSlide = document.getElementById('main-slide')
const container = document.getElementById('container')
const slideCount = mainSlide.querySelectorAll('div').length
let activeSlideIndex = 0
const height = container.clientHeight
console.log(height)

sidebar.style.top = `-${((slideCount - 1) * 100)}vh`

function changeSlide(direction) {
    if (direction === 'up') {
        console.log('нажата кнопка вверх');
        activeSlideIndex++
        if(activeSlideIndex === slideCount){
            activeSlideIndex = 0
        }
        console.log(activeSlideIndex);
    }

    else if (direction === 'down') {
        console.log('нажата кнопка вниз')
        activeSlideIndex--
        if(activeSlideIndex < 0){
            activeSlideIndex = slideCount-1
        }
        console.log(activeSlideIndex);
    }
    const height = container.clientHeight
    
    console.log(height)

    mainSlide.style.transform = `translateY(-${activeSlideIndex * height}px)`
    sidebar .style.transform = `translateY(${activeSlideIndex * height}px)`
}

upBtn.addEventListener('click', () => {
    changeSlide('up')
})

downBtn.addEventListener('click', () => {
    changeSlide('down')
})